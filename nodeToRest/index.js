/**

	Light sensing project - Serial to REST interface
	
	Authors: Jerome Abdelnour, Olivier Mailhot, Etienne Richan, Paulo Unia

**/
//Import libraries
var request = require('request');
var SerialPort = require('serialport');
const Readline = SerialPort.parsers.Readline;

//Serial port information
const serialPortName = "COM3";
const serialPortBaudRate = 9600


//Open the serial port
var port = new SerialPort(serialPortName, {
  baudRate: serialPortBaudRate
});


//Log errors if they happen
port.on('error', function(err) {
  console.log('Error: ', err.message);
})

//Set data parser
const parser = port.pipe(new Readline({ delimiter: '\r\n' }));

//Parser event - Receive data
parser.on('data', function(serialIn){
	
	console.log("Input: " +serialIn);
	
	//Split data in ID and two sensors values
	var sensorData = serialIn.split("-");
	
	console.log("Device ID: "+sensorData[0]+" S1: "+ sensorData[1] + " S2: " + sensorData[2]);
	
	//Build request for external server
	request.post('http://ec2-13-59-76-174.us-east-2.compute.amazonaws.com/measurement', {form:{DeviceID:sensorData[0], S1:sensorData[1],S2:sensorData[2]}})
	
});