/**
 * Light sensing device - V1.0
 * 
 * Authors: Jerome Abdelnour, Olivier Mailhot, Etienne Richan, Paulo Unia
 */
#include <Wire.h>
#include "rgb_lcd.h"

//Constants for the screen
rgb_lcd lcd;
const int colorWhite = 0;

//Input pins for light sensors
int lightSensor1 = A0;
int lightSensor2 = A1;

//Placeholder to read values from sensors
int sensor1Value = 0;
int sensor2Value = 0;
int sensor1Percentage = 0;
int sensor2Percentage = 0;

//Refresh rate of the device
int refreshRateMS = 250;

//Unique device ID - Each device needs to have an unique ID.
char uniqueId[6] = "LSD01"; 

//Strings for display and serial port
String firstLine = "Light Sensing V1";
char sensorReadings[16] = "S1:xxx% S2:xxx%";
char serialOutputBuffer[14] = "XXXXX-000-000";

//Backlight setting history - filtering
int backlightPast[5] = {255,255,255,255,255};

/**
 * Setup function - Initialization of the peripherals.
 * This function gets called first, as soon as the uC boots
 */
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  
  pinMode(LED_BUILTIN,OUTPUT);

  // Init LCD, set up the number of columns and rows
  lcd.begin(16, 2);
  lcd.setColor(colorWhite);
  lcd.print(firstLine);
}

/**
 * Loop function - This gets called continuously once the setup() function is done.
 */
void loop() {

  readLightSensors();
  sendSensorsSerial();
  blinkLeds();
  printSensorsLCD();
  setLCDBacklight();

}

/**
 * Function to blink the status led, and set the refresh rate
 */
void blinkLeds(){
  digitalWrite(LED_BUILTIN, HIGH);
  delay(refreshRateMS/2);
  digitalWrite(LED_BUILTIN, LOW);
  delay(refreshRateMS/2);
}

/**
 * Function that reads the analogue light sensors
 */
void readLightSensors(){
  //Read the analog port values
  sensor1Value = analogRead(lightSensor1);
  sensor2Value = analogRead(lightSensor2);

  //Compute percentage
  sensor1Percentage = (sensor1Value*100)/1023;
  sensor2Percentage = (sensor2Value*100)/1023;
}

/**
 * Function to print 
 */
void sendSensorsSerial(){
  //Build the string to send through the serial port
  sprintf(serialOutputBuffer,"%s-%u-%u",uniqueId,sensor1Percentage,sensor2Percentage);
  //Send the data
  Serial.println(serialOutputBuffer);
}

/**
 * Print the sensor data on the LCD
 */
void printSensorsLCD(){

  //Build the string to show in the LCD screen
  sprintf(sensorReadings,"S1:%3u%% S2:%3u%%",sensor1Percentage,sensor2Percentage);
  
  lcd.setCursor(0,1);
  lcd.print(sensorReadings);
}

/**
 * Configure the brightness of the LCD
 */
void setLCDBacklight(){
  //Compute the brightness depending on the sensor value
  int backlightValue = computeBrightness(sensor1Percentage);

  //Set the background color
  lcd.setRGB(backlightValue,backlightValue,backlightValue); 
}

/**
 * Fonction used to compute the brightness of the screen depending on one of the light sensors
 */
int computeBrightness(int sensorValue){

  //New brightness
  sensorValue = (int)255 * ((101.0-sensorValue)/100.0);

  //Filter to stabilize the brightness and avoid sudden changes
  long mean = 0;
  for(int i=1;i<5;i++){
    backlightPast[i-1] = backlightPast[i];
    mean += backlightPast[i];
  }
  backlightPast[4] = sensorValue;
  mean+= sensorValue;

  return mean/5;
}
