const cons = require('consolidate');

module.exports = {
    getView: function(viewPath,viewParams, callback){
        cons.underscore(viewPath, viewParams, function(err,mainContentHtml){
            if (err){
                throw err;
            }
            cons.underscore('views/header.html', {}, function(err,headerHtml){
                if (err){
                    throw err;
                }
                cons.underscore('views/sidebar.html', {}, function(err,sidebarHtml){
                    if (err){
                        throw err;
                    }
                    cons.underscore('views/main.html', {header : headerHtml, mainContent: mainContentHtml, sidebar: sidebarHtml}, function(err,fullView){
                        if (err){
                            throw err;
                        }
                        callback(fullView);
                    });
                });
            });
        });
    }
};