const Sequelize = require('sequelize');
const sequelize = new Sequelize('hackatown2018','root','r00tIsAm0f0', {
	host : '127.0.0.1',
	dialect : 'mysql',
	logging: false,
	pool : {
		max: 10,
		min: 0,
		acquire: 30000,
		idle: 20000
	},
});

sequelize.authenticate().then(function(){
	console.log('Connection has been established');
}).catch(function(err){
	console.error('Unable to connect to database : ',err);
});
