const Hapi = require('hapi');
const Path = require('path');
const Inert = require('inert');

const appHandler = require('./handlers/app.js');

const models = require('./models/index.js');



//models.sequelize.sync().then(function(){
    // Create a server with a host and port
    const server = new Hapi.Server();
    server.connection({
        host: '0.0.0.0',
        port: 80,
        routes: {
            files: {
                relativeTo: Path.join(__dirname,'static')
            }
        }
    });


    server.ext('onRequest', function(request, reply){
        request.models = models;
        reply.continue();
    });


    const provision = async () => {
        await server.register(Inert);

        //Static route
        server.route({
            method: 'GET',
            path: '/{param*}',
            handler: {
                directory : {
                    path : '.',
                    redirectToSlash: false,
                    index: true
                }
            }
        });

        // App
        server.route({
            method: 'GET',
            path:'/',
            handler: function (request, reply) {
                appHandler.mapView(request,reply)
            }
        });

        
        server.route({
            method: 'GET',
            path:'/building/{buildingId}/measurements/last',
            handler: function (request, reply) {
                appHandler.getLastMeasurement(request,reply)
            }
        });

        server.route({
            method: 'GET',
            path:'/building/{buildingId}/measurements/day/mean',
            handler: function (request, reply) {
                appHandler.getMeanDayMeasurements(request,reply)
            }
        });

        server.route({
            method: 'POST',
            path:'/measurement',
            handler: function (request, reply) {
                appHandler.storeMeasurement(request, reply);
            }
        });

        server.route({
            method: 'GET',
            path:'/seedbuildings1',
            handler: function (request, reply) {
                appHandler.seedBuildings1(request, reply);
            }
        });
        server.route({
            method: 'GET',
            path:'/seedbuildings2',
            handler: function (request, reply) {
                appHandler.seedBuildings2(request, reply);
            }
        });

        server.route({
            method: 'GET',
            path:'/building/small',
            handler: function (request, reply) {
                appHandler.getAllBuildingSmall(request, reply);
            }
        });

        server.route({
            method: 'GET',
            path:'/building/{buildingId}',
            handler: function (request, reply) {
                appHandler.getBuilding(request, reply);
            }
        });
        

        // Start the server
        await server.start(function(err) {

            if (err) {
                throw err;
            }
            console.log('Server running at:', server.info.uri);
        });
    };

    provision();
//});


