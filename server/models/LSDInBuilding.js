module.exports = function(sequelize, DataTypes) {
	var LSDInBuilding = sequelize.define("LSDInBuilding", {
		buildingId: {type: DataTypes.INTEGER, primaryKey : true },
		lsdId: {type: DataTypes.INTEGER, primaryKey : true },
		startDate: DataTypes.DATE,
		endDate: DataTypes.DATE,
		inBuilding: DataTypes.BOOLEAN
		});

	LSDInBuilding.associate = function(models){
		LSDInBuilding.belongsTo(models.Building,{foreignKey: 'id', sourceKey: 'buildingId'});
		LSDInBuilding.belongsTo(models.LSD,{foreignKey: 'id', sourceKey: 'lsdId'});
		
	};

	return LSDInBuilding;
};
