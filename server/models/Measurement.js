module.exports = function(sequelize, DataTypes) {
	var Measurement = sequelize.define("Measurement", {
		buildingId: {type: DataTypes.INTEGER, primaryKey: false},
		lsdId: {type: DataTypes.INTEGER, primaryKey: false},
		timestamp: {type:DataTypes.DATE, defaultValue: DataTypes.NOW },
		measure: {type:DataTypes.JSON}
		});

	Measurement.associate = function(models){
		Measurement.belongsTo(models.Building,{foreignKey: 'buildingId', sourceKey: 'id'});
		Measurement.belongsTo(models.LSD,{foreignKey: 'lsdId', sourceKey: 'id'});
	};

	return Measurement;
};
