module.exports = function(sequelize, DataTypes) {
	var LSD = sequelize.define("LSD", {
		id: {type: DataTypes.INTEGER, primaryKey:true, autoIncrement:true},
		});

	LSD.associate = function(models){
		LSD.hasMany(models.Measurement,{foreignKey: 'lsdId', sourceKey: 'id'});
	};

	return LSD;
};
