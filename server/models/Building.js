module.exports = function(sequelize, DataTypes) {
	var Building = sequelize.define("Building", {
		id : {type: DataTypes.INTEGER, primaryKey:true, autoIncrement:true},
		polygone: DataTypes.JSON,
		score: DataTypes.DOUBLE,
		lat: DataTypes.DOUBLE(16,13),
		long: DataTypes.DOUBLE(16,13),
		height: DataTypes.DOUBLE,
		surface: DataTypes.DOUBLE,
		usableSurface: DataTypes.DOUBLE,
		easeOfInstallation: DataTypes.DOUBLE,
		maxPanels: DataTypes.INTEGER,
		quality : DataTypes.DOUBLE,
		shadowed: DataTypes.BOOLEAN,
		address: DataTypes.STRING,
		consumption: DataTypes.DOUBLE
		}/*, {
  			getterMethods: {
			    activeDevice(callback) {
			      sequelize.models.LSDInBuilding.count({where:{buildingId:this.id}}).then(function(c){
			      	
			      });
			    }
			}
  		}*/);

	Building.associate = function(models){
		Building.hasMany(models.Measurement,{foreignKey: 'buildingId', sourceKey: 'id'});
		Building.hasMany(models.LSDInBuilding,{foreignKey:'buildingId',sourceKey: 'id'});
	};

	return Building;
};
