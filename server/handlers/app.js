const viewManager = require('../managers/View.js');
const _ = require('underscore');
const fs = require('fs');

module.exports = {
      mapView : function(request, reply){
        viewManager.getView('views/map.html', {} , function (html) {
          reply(html);
        });
      },

      storeMeasurement :function(request, reply){
        request.models.Measurement.create({buildingId:1, lsdId:1, measure: JSON.stringify([request.payload.S1,request.payload.S2]) });
        reply();
      },

      getLastMeasurement :function(request, reply){
        var latestMeasurement =  request.models.Measurement.findOne({
            where:{buildingId:request.params.buildingId},order:request.models.sequelize.literal('id DESC')
        }).get('measure').then(function(latestMeasurement){
            reply(JSON.parse(latestMeasurement));
        });

      },

      getMeanDayMeasurements :function(request, reply){
      	
        var meanDayMeasurements1 = [0, 0, 0, 0, 0, 0, 5, 10, 15, 25, 35, 50, 50, 50, 40, 40, 30, 20, 10, 5, 0, 0, 0, 0];
        var meanDayMeasurements2 = [0, 0, 0, 0, 0, 0, 2, 4, 6, 10, 12, 20, 20, 20, 16, 14, 10, 6, 4, 3, 0, 0, 0, 0];

        if(request.params.buildingId == 1)
        {
    		reply(meanDayMeasurements1);
    	}else if(request.params.buildingId == 2){
    		reply(meanDayMeasurements2);
    	}else{
    	    reply([]);
        }

      },

      seedBuildings1 : function(request, reply){
      	
        let rawdata = fs.readFileSync('/home/ubuntu/app/server/buildings_complete.json');  
		let data = JSON.parse(rawdata);  
		for (var key in data) {
            if (data.hasOwnProperty(key)) {
                let inPolygone = data[key].bounds_coordinates;
                let inScore = data[key].score;
                let inLat = data[key].centroid_coordinates[0];
                let inLong = data[key].centroid_coordinates[1];
                let inHeight = data[key].height;
                let inSurface = data[key].total_area;
                let inUsableSurface = data[key].usable_area;
                let inEaseOfInstallation = data[key].ease_of_installation;
                let inMaxPanels = data[key].max_panels;
                let inQuality = data[key].quality;
                let inShadowed = data[key].shadowed;

                let computedConsumption = inSurface * inHeight * 54;

                request.models.Building.create({
                	polygone: inPolygone,
					score: inScore,
					lat: inLat,
					long: inLong,
					height: inHeight,
					surface: inSurface,
					usableSurface: inUsableSurface,
					easeOfInstallation: inEaseOfInstallation,
					maxPanels: inMaxPanels,
					quality : inQuality,
					shadowed: inShadowed,
					consumption :computedConsumption
                });
            }
        }
        reply();
      },

       seedBuildings2 : function(request, reply){
      	
        let rawdata = fs.readFileSync('/home/ubuntu/app/server/buildings_complete_2.json');  
		let data = JSON.parse(rawdata);  
		for (var key in data) {
            if (data.hasOwnProperty(key)) {
                let inPolygone = data[key].bounds_coordinates;
                let inScore = data[key].score;
                let inLat = data[key].centroid_coordinates[0];
                let inLong = data[key].centroid_coordinates[1];
                let inHeight = data[key].height;
                let inSurface = data[key].total_area;
                let inUsableSurface = data[key].usable_area;
                let inEaseOfInstallation = data[key].ease_of_installation;
                let inMaxPanels = data[key].max_panels;
                let inQuality = data[key].quality;
                let inShadowed = data[key].shadowed;

                let computedConsumption = inSurface * inHeight * 54;

                request.models.Building.create({
                	polygone: inPolygone,
					score: inScore,
					lat: inLat,
					long: inLong,
					height: inHeight,
					surface: inSurface,
					usableSurface: inUsableSurface,
					easeOfInstallation: inEaseOfInstallation,
					maxPanels: inMaxPanels,
					quality : inQuality,
					shadowed: inShadowed,
					consumption :computedConsumption
                });
            }
        }
        reply();
      },

      getAllBuildingSmall :function(request, reply){
        var buildings =  request.models.Building.findAll({attributes: ['id', 'polygone','lat','long','address', 'maxPanels','shadowed','score','consumption','easeOfInstallation'],include: [ request.models.LSDInBuilding ]})
        .then(function(buildings){
            max = _.max(buildings,function(b){return b.get('maxPanels')}).get('maxPanels');
        	let ret = {max : max, buildings:buildings};

        	reply(ret);
        });
        
      },

      getBuilding :function(request, reply){
        request.models.Building.find({
            where:{id:request.params.buildingId},include: [ request.models.LSDInBuilding ] })
        .then(function(building){
            reply(building);
        });

      },


};