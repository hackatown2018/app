var MapManager = {
    settings: {
        mapId : "map",
        defaultZoom : 14,
        initialCoords : {
            lat: 45.524205,
            lng: -73.588451
        },
        route: {
            color: "#117AC0",
            opacity: 0.7,
            weight: 8
        },
        polygon: {
            strokeColor: '#000000',
            strokeOpacity: 1,
            strokeWeight: 1,
            fillOpacity: 0.7,
            goodSpot : {
                fillColor: '#53cd43'
            },
            badSpot : {
                fillColor: '#cd0100'
            }

        }
    },

    polygons: [],

    markers: [],

    GMap: undefined,

    GeoMarker: undefined,

    Geocoder: undefined,

    $currentMap: undefined,
    initMap : function() {
        // Create a map object and specify the DOM element for display.
        MapManager.GMap = new GMaps({
            el: '#map',
            lat: MapManager.settings.initialCoords.lat,
            lng: MapManager.settings.initialCoords.lng,
            disableDefaultUI: true,
            streetViewControl: false,
            clickableIcons: false,
            mapTypeId: google.maps.MapTypeId.HYBRID
        });

        // Disable 45 degree view
        MapManager.GMap.setTilt(0);

        MapManager.Geocoder = new google.maps.Geocoder;
    },

    drawPolygon: function(score,isShadowed, polygonCoords){
        console.log("Drawing polygon");
        /*var fillColor;

        // TODO : Use dynamic scale to choose color. EX : if score == 1, color is green. If score == 0, color is red. If in middle, more red of more green.
        if(score > 0.7){
            fillColor = MapManager.settings.polygon.goodSpot.fillColor;
        }else {
            fillColor = MapManager.settings.polygon.badSpot.fillColor;
        }*/

        var fillColor = Helper.getBuildingColor(score);

        var polygon = MapManager.GMap.drawPolygon({
            paths : polygonCoords,
            strokeColor: MapManager.settings.polygon.strokeColor,
            strokeOpacity: MapManager.settings.polygon.strokeOpacity,
            strokeWeight: MapManager.settings.polygon.strokeWeight,
            fillColor: fillColor,
            fillOpacity: MapManager.settings.polygon.fillOpacity
        });

        MapManager.polygons.push(polygon);

        // TODO : Add mouseover
        /* Events handling
        polygon.addListener("mouseover",function(){
            this.setOptions({fillColor: "#ff00e4"});
        });

        polygon.addListener("mouseout",function(){
            this.setOptions({fillColor: "#00e7ff"});
        });

        polygon.addListener('click', function(e){
            console.log("CLICKED !");
        });
        */

        return polygon;
    },
    addMarker: function(coords){
        // TODO : Change marker image
        var marker = MapManager.GMap.addMarker({
            lat : coords.lat,
            lng: coords.lng
        });

        MapManager.markers.push(marker);

        return marker;
    },
    setCenter: function(lat,lng,callback){
        MapManager.GMap.setCenter(lat,lng,callback);
    },
    getZoom: function(){
        return MapManager.GMap.getZoom();
    },
    setZoom: function(zoomValue){
        MapManager.GMap.setZoom(zoomValue);
    },
    reverseAddressLookup: function(lat,lng,callback){
        MapManager.Geocoder.geocode({location : {lat : lat, lng : lng}},function(results,status){
            if(status === 'OK'){
                var address = results[0].formatted_address.split(',')[0];
                if(callback){
                    callback(address);
                }
            }else{
                console.log("ERROR : Could not reverse geocode. Msg : "+status);
            }
        });
    }

};