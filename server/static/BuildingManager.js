var BuildingManager = {
    buildings: [],
    maxPanels: undefined,

    addBuildingToMap: function(buildingData){
        var polygon = MapManager.drawPolygon(buildingData.score, buildingData.shadowed, buildingData.polygone);

        if(buildingData.LSDInBuildings.length > 0 && buildingData.LSDInBuildings[0].inBuilding){
            SensorManager.addSensor({lat: buildingData.lat, lng: buildingData.lng});
        }

        polygon.addListener('click', function(e){
            // TODO : Listen for click event only once and rebind it after the sidebar is hidden

            BuildingManager.getBuildingInfos(buildingData.id,function(buildingInfos){
                MapManager.reverseAddressLookup(buildingInfos.lat, buildingInfos.long,function(address){
                    SensorManager.updateDayMeasurementMean(buildingInfos.id);

                     buildingData.address = address;
                    SidebarManager.fillBuildingInfos(buildingData);

                    if(buildingInfos.LSDInBuildings.length > 0){
                        SensorManager.startSensorLiveUpdate(buildingInfos.id);
                        SidebarManager.$el.one('sidebar-closed', function(e){
                            SensorManager.stopSensorLiveUpdate()
                        });
                    }

                    ChartManager.sensor.setData(buildingData.sensorData);
                    if(MapManager.getZoom() < 17){
                        MapManager.setZoom(17);
                    }
                    MapManager.setCenter(buildingInfos.lat,buildingInfos.long,function(){});
                    SidebarManager.show();
                });
            });
        });
    },
    getBuildingInfos: function(buildingId,callback){
        $.get('/building/'+buildingId, function(data){
             if(callback){
                 callback(data);
             }
        });
    },
    getAllBuildings: function(callback){
        $.get('/building/small', function(data){
            BuildingManager.maxPanels = data.max;
            var allBuildings = data.buildings;
            for(var i=0;i<allBuildings.length;i++){
                BuildingManager.addBuildingToMap(allBuildings[i]);
            }
        });
    },
    getInstallCost: function(easeOfInstallation, nbPanel){
        return (1-easeOfInstallation)*500 + 500 + 50*nbPanel;
    }

};