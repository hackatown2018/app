var Helper = {
    getBuildingColor: function(score){
        var minH = 40;
        var H = minH +score*15;
        var S = 100;
        var minL = 15;
        var L = minL+40*score;
        return "hsl("+H+","+S+"%,"+L+"%)";
    },
    getSensorColour: function(intensity){
        var H =45;
        var S = 0+60*intensity/100;
        var minL = 5;
        var L = 80*intensity/100 + minL;
        return "hsl("+H+","+S+"%,"+L+"%)";
    },
    getShadowColor: function(){
        var H =286;
        var S = 9;
        var L = 35;
        return "hsl("+H+","+S+"%,"+L+"%)";
    }
};