var ChartManager = {
    sensor: {
        config: undefined,
        chart: undefined,
        init: function () {
            var ctx = document.getElementById("sidebar-sensor-graph").getContext('2d');
            ChartManager.sensor.config = {
                type: 'line',
                data: {
                    labels: ['0h', '', '', '3h', '', '', '6h', '', '', '9h', '', '', '12h', '', '', '15h', '', '', '18h', '', '', '21h', '', '23h'],
                    datasets: [{
                        backgroundColor: '#9b4840',
                        borderColor: '#ff00e4',
                        data: [],
                        fill: false
                    }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                suggestedMax: 100
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Kilo Lumens'
                            }
                        }],
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Heure de la journée'
                            }
                        }]
                    }
                }
            };

            ChartManager.sensor.chart = new Chart(ctx, ChartManager.sensor.config);
        },
        setData: function (data) {
            ChartManager.sensor.config.data.datasets[0].data = data;
            ChartManager.sensor.chart.update();
        }
    },
    money : {
        config: undefined,
        chart: undefined,
        init: function () {
            var ctx = document.getElementById("sidebar-cost-graph").getContext('2d');
            ChartManager.money.config = {
                type: 'line',
                data: {
                    labels: ['0', '1', '2', '3', '4', '6', '7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25'],
                    datasets: [{
                        backgroundColor: '#9b4840',
                        borderColor: '#ce3131',
                        data: [],
                        fill: false,
                        label : "Cout (panneaux + installation)"
                    },{
                        backgroundColor: '#9b4840',
                        borderColor: '#2ba84a',
                        data: [],
                        fill: false,
                        label : "Retour sur invevestissement"
                    },
                        {
                            backgroundColor: '#9b4840',
                            borderColor: '#3bcbd6',
                            data: [],
                            fill: false,
                            label : "Retour avec ré-investissement"

                        }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                suggestedMax: 100
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Dollars CAD'
                            }
                        }],
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Année'
                            }
                        }]
                    }
                }
            };

            ChartManager.money.chart = new Chart(ctx, ChartManager.money.config);
        },
        setData: function (cost, returns, compound_returns) {
            ChartManager.money.config.data.datasets[0].data = cost;
            ChartManager.money.config.data.datasets[1].data = returns;
            ChartManager.money.config.data.datasets[2].data = compound_returns;

            ChartManager.money.chart.update();
        }
    },
    init: function(){
        ChartManager.sensor.init();
        ChartManager.money.init();
    }
};