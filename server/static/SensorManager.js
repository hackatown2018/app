var SensorManager = {
    liveUpdateTimer : undefined,
    liveUpdateInterval: 500,
    addSensor: function(position){
        var marker = MapManager.addMarker(position);

    },
    updateDayMeasurementMean: function(buildingId){
        $.get('/building/'+buildingId+'/measurements/day/mean',function(data){
            if(data.length > 0){
                SidebarManager.fillMeasurementSensor(data);
            }else{
                // TODO : Instead of a separate call, this should all be returned in the building infos call
            }
        });
    },
    updateLiveSensor: function(buildingId){
        $.get('/building/'+buildingId+'/measurements/last',function(data){
            // TODO : Add some data validation
            SidebarManager.fillLiveSensor({
                sensor1 : data[0],
                sensor2 : data[1]
            });
        });
    },
    startSensorLiveUpdate: function(buildingId){
        SensorManager.liveUpdateTimer = setInterval(function(){
            SensorManager.updateLiveSensor(buildingId);
        },SensorManager.liveUpdateInterval);
    },
    stopSensorLiveUpdate: function(){
        clearInterval(SensorManager.liveUpdateTimer);
        SensorManager.liveUpdateTimer = undefined;
    }
};