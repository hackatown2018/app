var SidebarManager = {
    $el: undefined,
    returns : [37.800000000000004, 74.655, 111.325725, 147.81309637500001, 184.11803089312502, 220.2414407386594, 256.1842335349661, 291.94731236729126, 327.5315758054548, 362.93791792642753, 398.1672283367954, 433.2203921951114, 468.09829023413585, 502.80179878296514, 537.3317897890503, 571.689130840105, 605.8746851859045, 639.8893117599749, 673.733865201175, 707.4091958751692, 740.9161498957933, 774.2555691463143, 807.4282913005827, 840.4351498440798, 873.2769740948594],
    counpoungReturns : [37.800000000000004, 76.20291, 115.9785416186775, 157.16785979320147, 199.81275970926652, 243.95607867851768, 289.6416079052038, 336.9141041337903, 385.81930116920904, 436.4039212613725, 488.7156863455388, 542.8033291300766, 598.7166040231475, 656.5062978898028, 716.2242406309692, 777.9233155757917, 841.6574696787919, 907.4817235133081, 975.4521810526844, 1045.6260392306986, 1118.0615972727342, 1192.8182657892362, 1269.9565756230186, 1349.538186442041, 1431.6258950693154],
    init: function(){
        SidebarManager.$el = $("#sidebar");
        SidebarManager.$el.find(".sidebar-close-btn").on('click',function(e){
            SidebarManager.hide();
        });

        SidebarManager.$el.find('#slider-number-panels').on('input', function(e){
             SidebarManager.$el.find('.sidebar-body-nb-panneaux > span').text(this.value);

            var cost = BuildingManager.getInstallCost(SidebarManager.$el.find(".sidebar-body").attr('easeOfInstall'),this.value);

            var realCost = [],
                realReturns = [],
                realCoumpond = [];

            for(var i=0;i <25;i++){
                realCost.push(cost);
                realReturns.push(SidebarManager.returns[i]*this.value);
                realCoumpond.push(SidebarManager.counpoungReturns[i]*this.value);
            }

            ChartManager.money.setData(realCost,realReturns, realCoumpond);
        });
    },
    isShowed: function(){
        return SidebarManager.$el.width() !== 0;
    },
    show: function(){
        if(SidebarManager.isShowed()){
            SidebarManager.$el.one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(e){
                SidebarManager.$el.width('30%');
            });

            SidebarManager.hide();
        }else{
            SidebarManager.$el.width('30%');
        }
    },
    hide: function(){
        var closeBtn = SidebarManager.$el.find('.sidebar-close-btn');
        closeBtn.css({display:'none'});
        SidebarManager.$el.one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(e){
            closeBtn.css({display:'block'});
        });
        SidebarManager.$el.width(0);

        SidebarManager.$el.trigger('sidebar-closed');
    },
    fillBuildingInfos: function(data){
        var $title = SidebarManager.$el.find('.sidebar-body-address-div'),
            $body = SidebarManager.$el.find('.sidebar-body'),
            $nbPanneaux = $body.find(".sidebar-body-nb-panneaux > span"),
            $energyConsumption = $body.find(".sidebar-body-consumption-div > span"),
            $sliderPanels = $body.find("#slider-number-panels"),
            $energyProduction = $body.find(".sidebar-body-production-div > span"),
            $actions = SidebarManager.$el.find('.sidebar-body-action-button');

        $body.attr('easeOfInstall', data.easeOfInstallation);

        var cost = BuildingManager.getInstallCost(data.easeOfInstallation,data.maxPanels),
            returns = SidebarManager.returns*data.maxPanels,
            coumpound = SidebarManager.counpoungReturns*data.maxPanels;

        var realCost = [],
            realReturns = [],
            realCoumpond = [];

        for(var i=0;i <25;i++){
            realCost.push(cost);
            realReturns.push(SidebarManager.returns[i]*data.maxPanels);
            realCoumpond.push(SidebarManager.counpoungReturns[i]*data.maxPanels);
        }

        ChartManager.money.setData(realCost,realReturns, realCoumpond);

        $sliderPanels.attr('max', data.maxPanels);
        $sliderPanels.attr('value',data.maxPanels);
        $nbPanneaux.text(data.maxPanels);

        var energyProduction = 378 * data.maxPanels;

        $title.text(data.address);
        $energyProduction.text(energyProduction.toString());
        $energyConsumption.text(data.consumption.toString());
    },
    fillLiveSensor: function(sensorData){
        var $sensor1 = SidebarManager.$el.find('#sensor-1-color-div'),
            $sensor2 = SidebarManager.$el.find('#sensor-2-color-div');

        $sensor1.text(sensorData.sensor1);
        $sensor2.text(sensorData.sensor2);

        $sensor1.css({
           'background-color': Helper.getSensorColour(sensorData.sensor1)
        });

        $sensor2.css({
            'background-color': Helper.getSensorColour(sensorData.sensor2)
        });
    },
    fillMeasurementSensor: function(sensorData){
        // TODO : Set data after the sidebar is fully appeared to see the chart animation
        ChartManager.sensor.setData(sensorData);
    }
};