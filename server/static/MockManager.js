var MockManager = {
    allBuilding: function(){
        MockManager.building(1);
        MockManager.building(2);
        MockManager.building(3);
        MockManager.building(4);
    },
    newBuilding: function(){
        var building = {
            "id": 1,
            "polygone": [
            [
                45.51862600500411,
                -73.59393881926069
            ],
            [
                45.51866283857565,
                -73.59402255476887
            ],
            [
                45.51854643987251,
                -73.59412614386198
            ],
            [
                45.518535155022114,
                -73.59412717465209
            ],
            [
                45.51850376437267,
                -73.59405581318858
            ],
            [
                45.518509444289045,
                -73.59404254898978
            ],
            [
                45.51862444714519,
                -73.59394019907768
            ]
        ],
            "score": 1,
            "lat": 45.5185725849199,
            "long": -73.5940361791621,
            "height": 82.122,
            "surface": 125.03698730468749,
            "usableSurface": 125.03698730468749,
            "easeOfInstallation": 1,
            "maxPanels": 43,
            "quality": 1,
            "shadowed": true,
            "address": null,
            "consumption": 554487.5234575194,
            "createdAt": "2018-01-21T13:42:56.000Z",
            "updatedAt": "2018-01-21T13:42:56.000Z",
            "LSDInBuildings": [
            {
                "buildingId": 1,
                "lsdId": 1,
                "startDate": "2018-01-21T08:44:14.000Z",
                "endDate": "2018-01-21T08:44:15.000Z",
                "inBuilding": true,
                "createdAt": "2018-01-21T08:44:18.000Z",
                "updatedAt": "2018-01-21T08:44:19.000Z",
                "id": null
            }
        ]
        };
        BuildingManager.addBuildingToMap(building);
        MapManager.setCenter(building.lat,building.long,function(){});
    },
    building: function(dataIndex){
        var buildingData;
        if(dataIndex === 1) {
            buildingData = {
                score: 1,
                position: {
                    lat: 45.504656,
                    lng: -73.613162
                },
                polygonPath: [
                    [45.505295, -73.613395],
                    [45.504367, -73.613914],
                    [45.504083, -73.612820],
                    [45.505021, -73.612286]
                ],
                infos: {
                    buildingAddress: '2900 edouard montpetit',
                    daylight: 8,
                    installCost: 600,
                    energyConsumption: 160,
                    expectedSavingPerMonth: 20,
                    returnOnInvestment: 6
                },
                sensorData: [0, 0, 0, 0, 0, 0, 5, 10, 15, 25, 35, 50, 50, 50, 40, 40, 30, 20, 10, 5, 0, 0, 0, 0]
            };
        }else if(dataIndex === 2){
            buildingData = {
                score: 0.5,
                position: {
                    lat: 45.504656,
                    lng: -73.613162
                },
                polygonPath: [
                    [45.512836920,-73.606698716],
                    [45.512790631,-73.606593999],
                    [45.512829511,-73.606547946],
                    [45.512888104,-73.606495543],
                    [45.512934195,-73.606599812],
                    [45.512878221,-73.606661780]
                ],
                infos: {
                    buildingAddress: 'Etienne 1',
                    daylight: 2,
                    installCost: 600,
                    energyConsumption: 20,
                    expectedSavingPerMonth: 5,
                    returnOnInvestment: 12
                },
                sensorData: []
            };
        }else if(dataIndex === 3){
            buildingData = {
                score: 0.5,
                position: {
                    lat: 45.504656,
                    lng: -73.613162
                },
                polygonPath: [
                    [45.514970160,-73.602597859],
                    [45.514836904,-73.602586520],
                    [45.514846907,-73.602348754],
                    [45.514980164,-73.602360093]
                ],
                infos: {
                    buildingAddress: 'Etienne 2',
                    daylight: 2,
                    installCost: 600,
                    energyConsumption: 20,
                    expectedSavingPerMonth: 5,
                    returnOnInvestment: 12
                },
                sensorData: []
            };
        }else{
            buildingData = {
                score: 0.1,
                position: {
                    lat: 45.508568,
                    lng: -73.617522
                },
                polygonPath: [
                    [45.508527, -73.617615],
                    [45.508652, -73.617508],
                    [45.508614, -73.617421],
                    [45.508490, -73.617529]
                ],
                infos: {
                    buildingAddress: '5637 Avenue de Stirling',
                    daylight: 2,
                    installCost: 600,
                    energyConsumption: 20,
                    expectedSavingPerMonth: 5,
                    returnOnInvestment: 12
                },
                sensorData: [0, 0, 0, 0, 0, 0, 2, 4, 6, 10, 12, 20, 20, 20, 16, 14, 10, 6, 4, 3, 0, 0, 0, 0]
            };
        }

        BuildingManager.addBuildingToMap(buildingData);
    },

    sensor: function(){
        var sensorData = {
            id: 1,
            position: {
                lat: 45.504656,
                lng: -73.613162
            },
            infos: {
                sensor1 : 44,
                sensor2: 66
            }
        };

        SensorManager.addSensor(sensorData);
    },
    polygon: function(){
        var testPolygonCoords = [
            [45.505295, -73.613395],
            [45.504367, -73.613914],
            [45.504083, -73.612820],
            [45.505021, -73.612286]
        ];

        var polygon = MapManager.drawPolygon(1,testPolygonCoords);

        polygon.addListener('click', function(e){
            console.log("CLICKED !");
        });

        /*google.maps.event.addListener(MapManager.GMap.map, event, callback);
        google.maps.event.addListener(polygon, "click", function() {
            console.debug('clicked on new poly');
        });*/
    },
    modal: function(){
        var mockedData = {
            buildingAddress : '3000 edouard mont-petit',
            daylight: 8,
            installCost: 600,
            energyConsumption: 160,
            expectedSavingPerMonth: 20,
            returnOnInvestment: 6
        };

        SidebarManager.fillBuildingInfos(mockedData);
        SidebarManager.show();
    },
    testLoadAllPolygon: function(){
        $.get('/O01_2013.json', function(data){
            var tempCoordsPath;
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    tempCoordsPath = data[key].bounds_coordinates;
                    MapManager.drawPolygon(1,tempCoordsPath);
                }
            }
        })
    }
};